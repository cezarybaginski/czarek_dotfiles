if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

#autoload -Uz mpv

zstyle ':completion:*:*:mpv:*' file-patterns '*.(#i)(flv|mp4|webm|mkv|wmv|mov|avi|mp3|ogg|wma|flac|wav|aiff|m4a|m4b|m4v|gif|ifo)(-.) *(-/):directories' '*:all-files'
zstyle ':completion:*:*:display:*' file-patterns '*.(#i)(png|jpg)(-.) *(-/):directories' '*:all-files'

my_eod(){
  strftime -r "%F %H:%M" "$(strftime '%F' $EPOCHSECONDS) 18:00"
}

my_time_left_colored(){
  local EOD=$(my_eod)

  local diff=$((EOD - EPOCHSECONDS))
  local minutes_left=$((diff / 60 ))

  local col_white=4
  local col_yellow=3
  local col_red=1

  local time_col=$col_white
  if (( $minutes_left < 0 )); then
    local time_col=$col_red
  elif (( $minutes_left < 60 )); then
    local time_col=$col_yellow
  fi
  echo "%B%F{${time_col}}${minutes_left}%f%b"
}

if [[ -s "${HOME}/.bin/tmuxinator.zsh" ]]; then
  source "${HOME}/.bin/tmuxinator.zsh"
fi

# {{{ History
#HISTFILE="${HOME}/.zhistory"
SAVEHIST=50000
HISTSIZE=52000

setopt appendhistory
setopt autocd extendedglob nomatch notify
setopt extendedhistory
setopt incappendhistory
setopt sharehistory
setopt histignorespace
setopt histverify
setopt histsavebycopy

unsetopt histignoredups
unsetopt histignorealldups
unsetopt histsavenodups
unsetopt histexpiredupsfirst

unsetopt autoremoveslash
unsetopt beep

setopt bsdecho
setopt correct_all
setopt histfcntllock
setopt histreduceblanks
setopt numericglobsort

setopt shoptionletters

##
#histnostore
#posixbuiltins
#printeightbit
#shwordsplit
#rcexpandparam

HIST_STAMPS="yyyy-mm-dd"
setopt hist_ignore_space # trick so that history doesn't get polluted
# }}}
#
# {{{ Editors, etc
export EDITOR='vim'
export VISUAL='vim'
export ALTERNATE_EDITOR='vim'
export MANPAGER="/usr/bin/less"
export PARINIT="w78 rTbgqR B=.,?_A_a Q=_s>|"
export BROWSER="/usr/bin/google-chrome"
export BC_ENV_ARGS=~/.bc
alias vimx='echo -en "\033]50;-*-terminus-*-*-*-*-14-*-72-72-*-*-*-*\007" && vim'
alias vim='LC_ALL=en_US.utf8 LC_ALL=en_US.utf8 vim'
alias vi='LC_ALL=en_US.utf8 LC_ALL=en_US.utf8 vim'
LC_CTYPE=en_US.utf8
export COLORFGBG="lightgray;black"
export PATH=$PATH:$HOME/.bin:$HOME/.local/bin
# }}}
# {{{ Keyboard
bindkey -s '\C-z' '\M-q edithist\n'
#bindkey '^R' history-incremental-search-backward
bindkey -v
bindkey "$(echotc kl)" backward-char
bindkey "$(echotc kr)" forward-char
bindkey "$(echotc ku)" up-line-or-history
bindkey "$(echotc kd)" down-line-or-history
bindkey "\e[3~" delete-char        # Delete
bindkey '\M-q' push-input # replaces push-line in 3.0.x
# }}}

# {{{ Important
compinit -d $HOME/.tmp/my_zcompdump

if grep -q docker /proc/1/cgroup; then # pick something that isn't buggy on docker
  #ZSH_THEME="clean"
  ZSH_THEME="afowler"
else
  # ZSH_THEME="$default_theme"
  # ZSH_THEME="random"
fi

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"
# }}}
# {{{ Git preferences
export GIT_SHELL='/bin/zsh'
export GIT_EDITOR='vim'
export GIT_RMAIL='mutt'
export GIT_VMSTAT='vmstat'
export GIT_PAGER='/usr/bin/less'
export GIT_BROWSER='elinks'
# }}}
# {{{ Generic stuff
alias be='bundle exec'
# Disabled. Use ~/.bin/cp instead
alias cp="nocorrect ${HOME}/.bin/cp -i"
# }}}
# {{{ more important stuff
if [ -n "$DISPLAY" ]; then
  #TODO: interferes with gnome
  xmodmap | grep -q Caps_Lock && xmodmap ~/.Xmodmap
  xset r rate 180 80
  xset m 1
fi
alias h='fc -li 1'
alias l='ls -lA'
cdpath=(. ~)
# }}}

GDBHISTSIZE=-1

# {{{ git stuff
#type g |grep -q "g is an alias for" && unalias g
#if whence -v g | grep -q "g is a shell function"; then
#  echo "g() is already defined: $(whence -c g)"
#fi
# }}}
# {{{ Other
compctl -c man nohup fakeroot whence wtfsck

# This is from Risto J. Laitinen
# Rmdir only real directories
compctl -g '*(D/)' rmdir

# This is from Risto J. Laitinen
# Cd/pushd only directories or symbolic links to directories
compctl -g '*(D-/)' cd pushd
# }}}
# {{{ clipboard
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
# }}}
# {{{ Colored filename-completion
ZLS_COLORS="$LS_COLORS"
export ZLS_COLORS
zmodload zsh/complist 2> /dev/null
# }}}
# {{{ tool aliases
alias icanhaz='sudo apt-get install'
alias wtf='whence -savc'
alias myx='startx -- -dpi 100 -depth 24 & disown && exit'
# }}}
# {{{ Git
alias d='git diff'
alias gwc='git whatchanged -p --abbrev-commit --pretty=medium'
# }}}
# {{{ ls
eval `dircolors ~/.dircolors 2> /dev/null `
if uname -o | grep -q -i Linux; then
  export LS_OPTIONS='-F --color=auto'
  [ -e /usr/bin/ack ] || alias ack='ack-grep'
else
  export LS_OPTIONS='-G'
fi
# }}}
# {{{ ls
# "(l)a(t)est"
alias lt="/bin/ls $LS_OPTIONS -lLrst"
# "(l)a(t)est (a)ll"
alias lta="/bin/ls $LS_OPTIONS -lLrsta"
# }}}
#fpath=($fpath $HOME/.zsh/func)
#typeset -U fpath

## For unicorn to save memory
##export WEB_CONCURRENCY=1
#
#export PATH=$PATH:$HOME/.bin:$HOME/.gocode/bin:/usr/local/go/bin:$HOME/.local/bin
#export UNCRUSTIFY_CONFIG=$HOME/.uncrustify.cfg
##export RUBYGEMS_GEMDEPS=-
# {{{ Misc
stty eof '^D'
stty erase '^?'
# }}}
#set eight on

#alias hp='git push heroku master'
#alias hc='heroku console'

alias dl='docker ps -l -q'
alias drm="docker rm"
alias dps="docker ps"

alias bc='bc --quiet --mathlib'
alias gap='git add -p'
alias mux=tmuxinator

alias jtags="ctags -R app config lib && sed -i '' -E '/^(if|switch|function|module\.exports|it|describe).+language:js$/d' tags"

gci(){
  read msg
  git commit -m "$msg"
}

# }}}
# {{{ Personal stuff
if [ -e ~/.zshrc_personal ]; then
  source ~/.zshrc_personal
fi
# }}}

alias -s {yml,yaml,cpp,h,hpp,txt,md}=vim
alias -g G='| grep'
alias dc='docker-compose'
alias dcr='docker-compose run'
alias dcb='docker-compose build'

hash direnv 2>/dev/null && eval "$(direnv hook zsh)"

function g() {
  if [[ $# = '0' ]]; then
    git status
  else
    git "$@"
  fi
}

alias dc=docker-compose

# TODO: put in your ~/.zshrc_personal
# export MY_UNIT_TEST_SIGNING_KEY=fe230592332395aheofiwhe

fpath+=${ZDOTDIR:-~}/.zsh_functions

export PATH=$HOME/.pyenv/versions/3.7.2/bin:$PATH

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

umask 027

# vi: fdl=1 ft=zsh:
