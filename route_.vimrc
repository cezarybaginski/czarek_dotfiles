" {{{ Compatibility
set nocompatible
" }}}
" {{{ Pathogen
filetype off
execute pathogen#infect()
" }}}
" {{{ Compatibility
syntax enable " syntax highlighting
" }}}
" {{{ Important
let mapleader =","
filetype plugin on
filetype plugin indent on
" }}}
" {{{ Colors
set t_Co=256 "for tmuxline theme bug when tmux uses 8bit solarize
"set t_Co=16 "for tmuxline theme bug when tmux uses 8bit solarize
" set t_ut= " disable Background Color Erase

let g:solarized_contrast = "normal"

"if strftime("%H") >= 16 && strftime("%H") <= 17
"  let g:solarized_termtrans = 0
"  set background=light
"else
  let g:solarized_termtrans = 1
  set background=dark
"endif



" colorscheme solarized

" MiniBufExpl Colors
hi MBENormal               guifg=#808080 guibg=fg
hi MBEChanged              guifg=#CD5907 guibg=fg
hi MBEVisibleNormal        guifg=#5DC2D6 guibg=fg
hi MBEVisibleChanged       guifg=#F1266F guibg=fg
hi MBEVisibleActiveNormal  guifg=#A6DB29 guibg=fg
hi MBEVisibleActiveChanged guifg=#F1266F guibg=fg

hi! link zshVariable Identifier
hi! Constant term=bold cterm=bold ctermfg=1 guifg=#ffa0a0

hi! OL1 ctermfg=2

" }}}
" {{{ Indenting
set autoindent " Automatically set the indent of a new line (local to buffer)
set smartindent " smartindent (local to buffer)
set cindent
" }}}
" {{{ Cursor highlights
set cursorline
"set cursorcolumn
" }}}
" {{{ Searching
set hlsearch  " highlight search
nmap <silent> ,/ :nohlsearch<CR>
set incsearch  " incremental search, search as you type
set ignorecase " Ignore case when searching
set smartcase " Ignore case when searching lowercase
" }}}
" {{{ Trailing whitespaces
map ¶ :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")')) <CR>:w<CR>
map þ :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")')) <CR>:w<CR>
autocmd FileType css,haml,html,sh,ruby,php,rdoc,python,cpp,javascript autocmd BufWritePre * :%s/\s\+$//e
:highlight ExtraWhitespace ctermbg=red guibg=red
:match ExtraWhitespace /\s\+$\| \+\ze\t/
" }}}
" {{{ File Stuff
autocmd FileType text setlocal textwidth=78
autocmd FileType mail :nmap  :w:!aspell -e -c %:e
" }}}
" {{{ Sessions
" Sets what is saved when you save a session
set sessionoptions=blank,buffers,curdir,folds,help,resize,tabpages,winsize
" }}}
" {{{ Match
" set matchpairs+=<:>
set showmatch  " Show matching brackets.
set matchtime=5  " Bracket blinking.
" }}}
" {{{ Mouse
"set mouse=a " Enable the mouse
"behave xterm
"set selectmode=mouse
set mousehide  " Hide mouse after chars typed
set mouse=
" }}}
" {{{ Cursor Movement
" Make cursor move by visual lines instead of file lines (when wrapping)
"map k gk
"ap j gj
"ap j gj
"ap E ge
set scrolloff=3
" }}}
" {{{ Folds
set foldmethod=marker
"set foldclose=all
"set foldopen=all
set foldlevel=1
set foldenable
nnoremap <space> za
" }}}
" {{{ Diff
set diffopt=filler,iwhite
" diffのコマンド
function! MyDiff()
  let opt = ""
  if &diffopt =~ "iwhite"
    let opt = opt . "-b "
  endif
  silent execute "!git-diff-normal-format " . opt . v:fname_in . " " . v:fname_new . " > " . v:fname_out
  redraw!
endfunction


" }}}
" {{{ Spell
let loaded_vimspell = 1
let spell_language_list = "english,polish"
let spell_auto_type="tex,doc,mail,txt,html,xhtml"
let g:spchklang = "pol"
set spelllang=en_us
" }}}
" {{{ Misc
set novisualbell " Turn off the bell, this could be more annoying, but I'm not sure how
set noerrorbells  " No noise.
set hidden
" set patchmode=.orig
set history=1000 " Number of things to remember in history.
"set cinoptions=:0,p0,t0
"set cinwords=if,else,while,do,for,switch,case
"runtime macros/matchit.vim
set wildmenu
set wildmode=list:longest
set wildchar=<Tab>
set wildcharm=<C-Z>

set nrformats=hex

set ttimeoutlen=50

if version >= 703
  if has("syntax")
  " set colorcolumn=78
  set colorcolumn=80
  :hi ColorColumn ctermbg=darkgrey guibg=darkgrey
  en
en
set fileencodings=utf-8,iso8859-2,cp1250,sjis,windows-1256
set modeline

set equalalways
set splitbelow splitright

"Vertical split then hop to new buffer
:noremap ,v :vsp^M^W^W<cr>
:noremap ,h :split^M^W^W<cr>

" set winfixheight
" set winfixwidth
" }}}
" {{{ open name with line
if exists('g:loaded_file_line') || (v:version < 700)
  finish
endif
let g:loaded_file_line = 1

function! s:gotoline()
  let file = bufname("%")
  " Accept file:line:column: or file:line:column and file:line also
  let names =  matchlist( file, '\(.\{-1,}\):\(\d\+\)\(:\(\d*\):\?\)\?$')

  if len(names) != 0 && filereadable(names[1])
    let l:bufn = bufnr("%")
    exec "keepalt edit " . names[1]
    exec ":" . names[2]
    exec ":bwipeout " l:bufn
    if foldlevel(names[2]) > 0
      exec ":foldopen!"
    endif

    if (names[4] != '')
      exec "normal! " . names[4] . '|'
    endif
  endif

endfunction

autocmd! BufNewFile *:* nested call s:gotoline()
" }}} open name with line
" {{{ per project .vimrc
set exrc
set secure
" }}}
" {{{ sudo write with w!!
cmap w!! %!sudo tee > /dev/null %
" }}}
" {{{ formatting
set pastetoggle=<F2>
set formatprg="par 78"
map Q gq

func! WordProcessorMode()
  setlocal formatoptions=1
  setlocal noexpandtab
  map j gj
  map k gk
  setlocal spell spelllang=en_us
  set thesaurus+=~/.vim/bundle/mthesaur.txt
  set complete+=s
  set formatprg=par
  setlocal wrap
  setlocal linebreak
endfu
com! WP call WordProcessorMode()
" }}}
" {{{ Python
"autocmd BufWritePost *.py call Flake8()
"let g:flake8_ignore="E501"
" let g:flake8_ignore="E501,W293"
" }}}

" {{{ Other file types
autocmd BufNewFile,BufRead *.functions set filetype=sh
autocmd BufNewFile,BufRead *.otl set filetype=votl
" }}}


" {{{ vim jump to last position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
" }}}
" {{{ plugins
" let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let airline#extensions#tmuxline#color_template = 'normal'
let g:airline#extensions#ale#enabled = 1
" }}}
" {{{ Unite
let g:unite_source_history_yank_enable = 1
call unite#filters#matcher_default#use(['matcher_fuzzy'])
nnoremap <leader>t :<C-u>Unite -no-split -buffer-name=files   -start-insert file_rec/async:!<cr>
nnoremap <leader>f :<C-u>Unite -no-split -buffer-name=files   -start-insert file<cr>
nnoremap <leader>r :<C-u>Unite -no-split -buffer-name=mru     -start-insert file_mru<cr>
nnoremap <leader>o :<C-u>Unite -no-split -buffer-name=outline -start-insert outline<cr>
nnoremap <leader>y :<C-u>Unite -no-split -buffer-name=yank    history/yank<cr>
nnoremap <leader>e :<C-u>Unite -no-split -buffer-name=buffer  buffer<cr>

" Custom mappings for the unite buffer
autocmd FileType unite call s:unite_settings()
function! s:unite_settings()
  " Play nice with supertab
  let b:SuperTabDisabled=1
  " Enable navigation with control-j and control-k in insert mode
  imap <buffer> <C-j>   <Plug>(unite_select_next_line)
  imap <buffer> <C-k>   <Plug>(unite_select_previous_line)
endfunction
" }}}
" {{{ auto solarize
"Source ~/.vimrc any time you save a file, so the background color will change if it becomes day/night.
"if has("autocmd")
"  autocmd bufwritepost * source ~/.vimrc
"endif
" }}}
" {{{ stuff
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2
nmap <ESC>s <ESC><ESC>:w<CR>
imap <ESC>s <ESC><ESC>:w<CR>
map <ESC>s <ESC><ESC>:w<CR>
imap <ESC>m <ESC><ESC>:w<CR>:mak %<CR>
map <ESC>m <ESC><ESC>:!tup stop<CR>:w<CR>:Dispatch<CR>

set showcmd
set autowrite

nmap <F8> :TagbarToggle<CR>

let g:joy_pure = 1
" let g:joy_mixed = 1
" :unlet g:joy_pure
" :unmap <Tab>

" au Syntax source ~/.vim/bundle/vimoutliner/syntax/votl.vim

" console speed
set ttyfast
set lazyredraw

"let g:syntastic_debug = 1
"let g:syntastic_debug_file = "/tmp/syntastic.log"

"" profiling
":profile start profile.log
":profile func *
":profile file *
"" At this point do slow actions
":profile pause
":noautocmd qall!

"au BufNewFile,BufRead,BufEnter *.cpp,*.hpp,*.cc,*.h set omnifunc=omni#cpp#complete#Main
map <F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" OmniCppComplete
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview

au BufNewFile,BufRead Tupfile,*.tup setf tup
" let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]
"let g:ycm_extra_conf_globlist = ['~/workspace/reference_legacy/*']
let g:ycm_always_populate_location_list = 1
" let g:ycm_confirm_extra_conf = 0
let g:ycm_collect_identifiers_from_tags_files = 1

map <C-K> :py3f /usr/share/vim/addons/syntax/clang-format-9.py<cr>
imap <C-K> <c-o> :py3f /usr/share/vim/addons/syntax/clang-format-9.py<cr>


"inoremap jk <Esc>

"colorscheme detailed
"colorscheme twilighted
"colorscheme elflord
colorscheme apprentice
hi Normal ctermbg=none

:nnoremap <leader>jf :YcmCompleter GoToInclude<CR>
:nnoremap <leader>jt :YcmCompleter GoTo<CR>
:nnoremap <leader>jx :YcmCompleter FixIt<CR>
:nnoremap <leader>jd :YcmCompleter GoToDefinition<CR>
:nnoremap <leader>je :YcmCompleter GoToDeclaration<CR>

noremap <leader>ji :py3f /usr/lib/llvm-9/share/clang/clang-include-fixer.py<cr>
noremap <leader>jr :wa<cr>:py3f /usr/lib/llvm-9/share/clang/clang-rename.py<cr>
noremap <leader>ja :wa<cr>:py3f /usr/lib/llvm-9/share/clang/run-find-all-symbols.py<cr>

"ClearCompilationFlagCache
"FixIt
"GetDoc
"GetDocImprecise
"GetParent
"GetType
"GetTypeImprecise
"GoTo
"GoToDeclaration
"GoToDefinition
"GoToImprecise
"GoToInclude

autocmd FileType c nnoremap <buffer> <silent> <C-]> :YcmCompleter GoTo<cr>

"nnoremap <ESC>a <ESC>:Ack
"nnoremap <ESC>a <ESC><ESC>:wa<CR>:Ack "\b<C-R><C-W>\b"<CR>:resize 10<CR>:cw<CR>
nnoremap <ESC>a <ESC>:Ack "\b<C-R><C-W>\b"<CR>:resize 10<CR>:cw<CR>

"nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>
"map <ESC>a <ESC><ESC>:wa<CR>


map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

hi link Exception Operator
hi Type ctermfg=7 term=bold
hi link cppStructure NonText
hi link cppModifier Operator
hi link cStorageClass Operator

map !s :.!bc -iql<CR>

" comment to avoid wrap
"set formatoptions=qroctmB
"
"default is formatoptions=croql
"-t -m -B +l

" -------------------------------
" In other words, this check occurs whenever you enter a buffer, move the cursor,
" or just wait without doing anything for 'updatetime' milliseconds.
"
" Normally it will ask you if you want to load the file, even if you haven't made
" any changes in vim. This can get annoying, however, if you frequently need to reload
" the file, so if you would rather have it to reload the buffer *without*
" prompting you, add a bang (!) after the command (WatchForChanges!).
" This will set the autoread option for that buffer in addition to setting up the
" autocommands.
"
" If you want to turn *off* watching for the buffer, just call the command again while
" in the same buffer. Each time you call the command it will toggle between on and off.
"
" WatchForChanges sets autocommands that are triggered while in *any* buffer.
" If you want vim to only check for changes to that buffer while editing the buffer
" that is being watched, use WatchForChangesWhileInThisBuffer instead.
"
command! -bang WatchForChanges                  :call WatchForChanges(@%,  {'toggle': 1, 'autoread': <bang>0})
command! -bang WatchForChangesWhileInThisBuffer :call WatchForChanges(@%,  {'toggle': 1, 'autoread': <bang>0, 'while_in_this_buffer_only': 1})
command! -bang WatchForChangesAllFile           :call WatchForChanges('*', {'toggle': 1, 'autoread': <bang>0})
" WatchForChanges function
"
" This is used by the WatchForChanges* commands, but it can also be
" useful to call this from scripts. For example, if your script executes a
" long-running process, you can have your script run that long-running process
" in the background so that you can continue editing other files, redirects its
" output to a file, and open the file in another buffer that keeps reloading itself
" as more output from the long-running command becomes available.
"
" Arguments:
" * bufname: The name of the buffer/file to watch for changes.
"     Use '*' to watch all files.
" * options (optional): A Dict object with any of the following keys:
"   * autoread: If set to 1, causes autoread option to be turned on for the buffer in
"     addition to setting up the autocommands.
"   * toggle: If set to 1, causes this behavior to toggle between on and off.
"     Mostly useful for mappings and commands. In scripts, you probably want to
"     explicitly enable or disable it.
"   * disable: If set to 1, turns off this behavior (removes the autocommand group).
"   * while_in_this_buffer_only: If set to 0 (default), the events will be triggered no matter which
"     buffer you are editing. (Only the specified buffer will be checked for changes,
"     though, still.) If set to 1, the events will only be triggered while
"     editing the specified buffer.
"   * more_events: If set to 1 (the default), creates autocommands for the events
"     listed above. Set to 0 to not create autocommands for CursorMoved, CursorMovedI,
"     (Presumably, having too much going on for those events could slow things down,
"     since they are triggered so frequently...)
function! WatchForChanges(bufname, ...)
  " Figure out which options are in effect
  if a:bufname == '*'
    let id = 'WatchForChanges'.'AnyBuffer'
    " If you try to do checktime *, you'll get E93: More than one match for * is given
    let bufspec = ''
  else
    if bufnr(a:bufname) == -1
      echoerr "Buffer " . a:bufname . " doesn't exist"
      return
    end
    let id = 'WatchForChanges'.bufnr(a:bufname)
    let bufspec = a:bufname
  end
  if len(a:000) == 0
    let options = {}
  else
    if type(a:1) == type({})
      let options = a:1
    else
      echoerr "Argument must be a Dict"
    end
  end
  let autoread    = has_key(options, 'autoread')    ? options['autoread']    : 0
  let toggle      = has_key(options, 'toggle')      ? options['toggle']      : 0
  let disable     = has_key(options, 'disable')     ? options['disable']     : 0
  let more_events = has_key(options, 'more_events') ? options['more_events'] : 1
  let while_in_this_buffer_only = has_key(options, 'while_in_this_buffer_only') ? options['while_in_this_buffer_only'] : 0
  if while_in_this_buffer_only
    let event_bufspec = a:bufname
  else
    let event_bufspec = '*'
  end
  let reg_saved = @"
  "let autoread_saved = &autoread
  let msg = "\n"
  " Check to see if the autocommand already exists
  redir @"
    silent! exec 'au '.id
  redir END
  let l:defined = (@" !~ 'E216: No such group or event:')
  " If not yet defined...
  if !l:defined
    if l:autoread
      let msg = msg . 'Autoread enabled - '
      if a:bufname == '*'
        set autoread
      else
        setlocal autoread
      end
    end
    silent! exec 'augroup '.id
      if a:bufname != '*'
        "exec "au BufDelete    ".a:bufname . " :silent! au! ".id . " | silent! augroup! ".id
        "exec "au BufDelete    ".a:bufname . " :echomsg 'Removing autocommands for ".id."' | au! ".id . " | augroup! ".id
        exec "au BufDelete    ".a:bufname . " execute 'au! ".id."' | execute 'augroup! ".id."'"
      end
        exec "au BufEnter     ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHold   ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHoldI  ".event_bufspec . " :checktime ".bufspec
      " The following events might slow things down so we provide a way to disable them...
      " vim docs warn:
      "   Careful: Don't do anything that the user does
      "   not expect or that is slow.
      if more_events
        exec "au CursorMoved  ".event_bufspec . " :checktime ".bufspec
        exec "au CursorMovedI ".event_bufspec . " :checktime ".bufspec
      end
    augroup END
    let msg = msg . 'Now watching ' . bufspec . ' for external updates...'
  end
  " If they want to disable it, or it is defined and they want to toggle it,
  if l:disable || (l:toggle && l:defined)
    if l:autoread
      let msg = msg . 'Autoread disabled - '
      if a:bufname == '*'
        set noautoread
      else
        setlocal noautoread
      end
    end
    " Using an autogroup allows us to remove it easily with the following
    " command. If we do not use an autogroup, we cannot remove this
    " single :checktime command
    " augroup! checkforupdates
    silent! exec 'au! '.id
    silent! exec 'augroup! '.id
    let msg = msg . 'No longer watching ' . bufspec . ' for external updates.'
  elseif l:defined
    let msg = msg . 'Already watching ' . bufspec . ' for external updates'
  end
  echo msg
  let @"=reg_saved
endfunction

let autoreadargs={'autoread':1}
"set diffexpr=MyDiff()

let g:vimrubocop_rubocop_cmd = 'bin/rubocop '
let g:airline_section_y = 'B%{bufnr("%")}'

highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=22 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=52 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=10 ctermbg=8 gui=none guifg=bg guibg=Red

let g:signify_vcs_list = ['git']
let g:signify_realtime = 0

" }}}
" {{{ ALE
"\   'javascript': ['eslint', 'flow-language-server'],
let g:ale_linters = {
      \   '*': ['remove_trailing_lines', 'trim_whitespace'],
      \   'json': [ 'prettier'],
      \   'ruby': ['rubocop'],
      \}


"\   'javascript': [ 'prettier', 'eslint'],

let g:ale_fixers = {
      \   '*': ['remove_trailing_lines', 'trim_whitespace'],
      \   'json': [ 'prettier'],
      \   'ruby': ['rubocop'],
      \}

let g:ale_sign_error = '❌'
let g:ale_sign_warning = '⚠️'

let g:ale_fix_on_save = 1

cabbr <expr> %% expand('%:p:h')

"hi Error ctermfg=16 ctermbg=9 term=reverse
"hi SpellBad ctermbg=1 ctermfg=11
"hi SpellCap ctermfg=black

"nmap <leader>aj :ALENext<cr>
"nmap <leader>ak :ALEPrevious<cr>
"nmap <leader>ad :ALEGoToDefinition<cr>

function ALELSPMappings()
    let lsp_found=0
    for linter in ale#linter#Get(&filetype)
        if !empty(linter.lsp) && ale#lsp_linter#CheckWithLSP(bufnr(''), linter)
            let lsp_found=1
        endif
    endfor
    if (lsp_found)
        nnoremap <buffer> K :ALEDocumentation<cr>
        nnoremap <buffer> gr :ALEFindReferences<cr>
        nnoremap <buffer> gd :ALEGoToDefinition<cr>
        nnoremap <buffer> gy :ALEGoToTypeDefinition<cr>
        nnoremap <buffer> gh :ALEHover<cr>

        setlocal omnifunc=ale#completion#OmniFunc
    endif
endfunction

autocmd BufRead,FileType * call ALELSPMappings()

" Write this in your vimrc file
" let g:ale_set_loclist = 0
" let g:ale_set_quickfix = 1

"}}}
"{{{ Other
if executable('ag')
  "let g:ackprg = 'ag --nogroup --nocolor --column'
  let g:ackprg = "ag --vimgrep --nogroup --nocolor --column --ignore '*:p'"
  " set grepprg=ag\ --vimgrep\ $*
  " set grepformat=%f:%l:%c:%m
  " let g:ackprg = 'ag --nogroup --nocolor --column'
else
endif

let g:ack_use_dispatch = 1

"set <F2>=[12~
"set <F3>=[13~
"set <F4>=[14~
"set <S-F4>=[26~

map <F3> :cprevious<CR>
map <S-F4> :cprevious<CR>
map <F4> :cnext<CR>

map <C-N> :next<cr>
map <C-P> :prev<cr>

:nnoremap <leader>an :cnext<cr>
:nnoremap <leader>ap :cprevious<cr>

autocmd FileType html autocmd BufWritePre <buffer> %s/\s\+$//e


autocmd FileType ruby,cucumber set noswapfile
autocmd FileType ruby,cucumber set backupcopy=auto,breakhardlink
autocmd FileType javascript set backupcopy=auto,breakhardlink
"autocmd FileType ruby,cucumber set backupdir=''

let g:miniBufExplAutoStart = 0

"}}}

" vi: set ts=2 sw=2 sts=2 expandtab fdl=0:
