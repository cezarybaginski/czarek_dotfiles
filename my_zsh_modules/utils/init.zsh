# {{{ default permissions
umask 027
# }}}
# {{{ audio with volume
play_volumed(){
	local volname="$1"
	mpv "--volume=$(volume_for "$volname" 20)" --no-video --really-quiet  "$@"
}

play_volumed_background(){
	local volname="$1"
	shift; play_volumed "$volname" --no-terminal "$@" &!
}

# non-interactive, silent mode
play_batch() {
	play_volumed_background batch "$@"
}

play_beep() {
	play_volumed_background beep "$@"
}
# }}}
# {{{ audio conversion
to_wav() {
  infile="${1:-$(now_playing)}"
  outfile="$(basename "${infile}.wav")"
  mpv --no-video --no-terminal --ao=pcm "--ao-pcm-file=$outfile" "$infile"
}

export BIG_TMP="$HOME/.cache"

to_mp3()
{
  infile="$1"
  [ -n "$infile" ] || { echo "Usage: <input> <output>"; return 1; }
  tmpdir=$(mktemp -d "$BIG_TMP/x200.XXXXX") || { echo "failed to create temp file"; return 1; }
  tmpfile="$tmpdir/output.wav"
  set -e
  mpv --no-video --no-terminal --ao=pcm "--ao-pcm-file=$tmpfile" "$infile"
  lame "${tmpfile}" "${infile}.mp3"
  rm -f "$tmpfile"
  rmdir "$tmpdir"
}

to_voice_mp3()
{
  infile="$1"
  [ -n "$infile" ] || { echo "Usage: <input> <output>"; return 1; }
  tmpdir=$(mktemp -d "$BIG_TMP/x200.XXXXX") || { echo "failed to create temp file"; return 1; }
  tmpfile="$tmpdir/output.wav"
  set -e
  mpv --no-video --no-terminal --ao=pcm "--ao-pcm-file=$tmpfile" "$infile"
  lame --preset voice -q 0 -V 8 "${tmpfile}" "${infile}.mp3"
  # -V 8 –v -q 0
  rm -f "$tmpfile"
  rmdir "$tmpdir"
}

enc_lame() {
  #  Encode wav to HQ mp3
  nice -n 20 lame -b 320 -m j -h "$@"
}

x200() {
	local infile=$1
	[ -n "$infile" ] || { echo "Usage: <input> <output>"; return 1; }
	local outfile="$2"
	[ -n "$outfile"] || outfile="$infile.x200.mp3"

	tmpdir=$(mktemp -d "$BIG_TMP/x200.XXXXX") || { echo "failed to create temp file"; return 1; }
	tmpfile="$tmpdir/output.x200.wav"
	tmpfile2="$tmpdir/output2.x200.wav"
	mkfifo "$tmpfile"
	mkfifo "$tmpfile2"
	mpv --no-video --no-terminal --ao=pcm "--ao-pcm-file=$tmpfile" --speed=2.0 "$infile" &
	cat "$tmpfile" | sox - -b16 "$tmpfile2" &
	lame "$tmpfile2" "$outfile"
	rm -f "$tmpfile"
	rm -f "$tmpfile2"
	rmdir "$tmpdir"
}

# }}}
# {{{ adding to playlists
now_playing() {
  /usr/bin/lsof -n -a -c mpv -Fn 2> /dev/null | /bin/grep '^n/' | /bin/grep -v "^n/\(usr\|dev\|var\|lib\|.cache/mesa_shader_cache/\|cache/czarek/dotfiles/.cache/fontconfig\)/\|^n/\(i915\|memfd:\(pulseaudio\|xshmfence\)\|\)$\|^n${HOME}\($\|/.cache\)" | sort | uniq | cut -dn -f2- | xargs  -d '\n' file -N -F'|' | /bin/grep -v '| directory$' | cut -d'|' -f1 | grep -v '\.jpg$'
}

safe_copy_music_now_playing() {
  [ -n "$1" ] || { echo "TARGET DIR NOT SET!!"; return 1; }

  target_dir="$1"
  mkdir -p "$target_dir"
  local file="$(now_playing)"
  [ -n "${file}" ] || return

  out_file_name=`basename "${FILE}"`
  echo "Now playing: ${out_file_name}"
  echo
  echo "(${FILE})"
  echo

  OUTPUT_FILE_PATH="${target_dir}/${out_file_name}"

  if [ -e "${OUTPUT_FILE_PATH}" ]; then
    if ! cmp "${OUTPUT_FILE_PATH}" "${FILE}"; then
      echo "FILENAME WITH SAME NAME BUT DIFFERENT CONTENTS EXISTS!!."
    else
      echo "File already copied. You must like it a lot."
    fi
    return 1
  fi

  echo Press ENTER to copy
  read ANS

  [ -e "$FILE" ] || { echo "File does not exist any more: ${FILE}"; return 1; }

  /bin/cp -- "${FILE}" "${target_dir}" || { echo "Copy FAILED: [${FILE}]"; return 1; }
}

copy_to_motiv() {
  safe_copy_music_now_playing "/home/Music"
}

copy_to_best() {
  safe_copy_music_now_playing "/home/Music/best"
}

copy_to_background() {
  safe_copy_music_now_playing "/home/Music/background"
}
# }}}
# {{{ media in archives

play_zip() {
	[ -n "$1" ] || { echo "No files given"; return 1; }

	tempdir="$(mktemp -d "$BIG_TMP/play_zip.XXXXX")" || { echo "failed to create tmp dir";	return 1;  }
	filename=$(realpath "$@")
	pushd "$tempdir"
	unzip "$filename"
	find -type f | sort -n > playlist.txt
	cat playlist.txt
	mpv --playlist playlist.txt
	popd
	rm -Rf "$tempdir"
}

play_dir() {
  screen_name=$1; shift

  # TODO: does not work with renamed sessions
  restore_single_run $screen_name && return 0

  vol=$1: shift
  dir=$1; shift

  #NOTE: sets volume
  #[ -z "${NOJACK}" ] && jack_with_retry
  if ! playlist=$(mktemp "/tmp/$0.playlist.XXXXXX"); then
    echo "failed to create tempfile"
    return 1
  fi

  find "${dir}/" -type f >> "${playlist}" || { sleep 1; echo "error finding files"; return 1; }



  #-af volume=${vol},channels=2:2:0:1:1:0
  single_run $screen_name mpv "--volume=${vol}" --quiet --no-video --shuffle --playlist="${playlist} $@" || { sleep 1; echo "error running mpv"; return 1; }
}
# }}}
# {{{ Docker alias
da () {
  docker start $1 && docker attach $1
}
# }}}
# {{{ Mounting of CD-ROM etc.

mountcd()
{
  # mount directory and go there
  if test "$1" = "" -o "$#" = "0"; then
    MOUNTCDDIR="/mnt/cdrom"
  else
    MOUNTCDDIR="$1"
  fi
  mount "$MOUNTCDDIR" && cd "$MOUNTCDDIR"
}

umountcd()
{
  if test "$1" = "" -o "$#" = "0"; then
    UMOUNTCDDIR="/mnt/cdrom"
  else
    UMOUNTCDDIR="$1"
  fi

  if pwd | $GREP $UMOUNTCDDIR > /dev/null 2>&1; then
    cd
  fi

  umount $UMOUNTCDDIR || return

  if echo $UMOUNTCDDIR | $GREP -E '.*cdrom.*|.*dvd.*' > /dev/null 2>&1; then
    sleep 1 && eject "$UMOUNTCDDIR"
  fi
}

# }}}
# {{{ Misc
date() {
  if [ $# -gt 0 ]; then
    /bin/date "$@"
  else
    /bin/date +"%Y.%m.%d %H:%M:%S"
  fi
}

edithist() {
  local tmp=${TMPPREFIX}${$}hist
  read -Erz >| "$tmp"
  "$EDITOR" "$tmp"
  print -Rz - "$(<$tmp)"
  rm -f "$tmp"
}

info() {
  nocorrect /usr/bin/info --vi-keys "$@"
}
# }}}
# {{{ Workspace
m()
{
  local name=${1:-$(basename $(pwd))}
  mux start "$name"
}
# }}}
# {{{ CD mount/get
get_dvd_dir() {
  RET="$( /bin/grep "cdrom\|cdrec" /etc/fstab | head -n 1 | xargs | cut -f2 -d' ' )"
  [ -n "$RET" ] || { echo "NO CD DIR!"; return 1; }
  echo ${RET}
}

get_dvd_dev() {
  RET="$( /bin/grep "cdrom\|cdrec" /etc/fstab| head -n 1 | xargs | cut -f1 -d' ' )"
  [ -n "$RET" ] || { echo "NO CD DEV!"; return 1; }
  echo ${RET}
}

cd2hd() {
  cdmountdir="$(get_dvd_dir)"
  # copy the contents of the cd, then ejects
  mount ${cdmountdir}
  selected="$@"
  echo "ls 1"
  ls -l ${cdmountdir}/
  sleep 3
  echo "ls 1"
  ls -l ${cdmountdir}/
  if [ -n "${selected}" ]; then
    nice -n 20 cp -Rv ${cdmountdir}/${selected} . && echo "ALL FILES OK!!!"
  else
    nice -n 20 cp -Rv ${cdmountdir}/* . && echo "ALL FILES OK!!!"
  fi
  CDDEV="$(get_dvd_dev)" eject
}
# }}}
# {{{ Volume control
volume_for() {
  name=$1
  local volume=${2:-0}
  local vol_dir="${HOME}/.volume"
  mkdir -p "$vol_dir"
  local old_filename="${vol_dir}/.${name}"
  local new_filename="${vol_dir}/${name}"
  if [ -e "${old_filename}" ]; then
    mv "${old_filename}" "${new_filename}"
    volume=$(cat "${new_filename}")
  elif [ -e "${new_filename}" ]; then
    volume=$(cat "${new_filename}")
  else
    echo "${volume}" > "${new_filename}"
  fi
  echo "${volume}"
}

set_volume() {
  control="$1"
  shift
  level="$1"
  shift
  option="$1"

  if [ -z "${option}" ]; then
    case "${level}" in
      [0-9]*)
        option="unmute"
        ;;
      *)
        option="${level}"
        level=""
        ;;
    esac
  fi

  [ -z "${level}" ] || levels="${level}%,${level}%"

  MSG=$(/usr/bin/amixer -c 0 sset ${control},0 ${levels} ${option} 2>&1) || { echo "ERROR: amixer: $MSG"; return 1; }
}

set_volumes() {
  value=$1

  for k in Master PCM Headphone Front Surround Center Side LFE; do
    set_volume "$k" "$value"
  done
}

volume.max() {
  set_volumes 95
}

volume.normal() {
  set_volumes 80
}

volume.quiet() {
  set_volumes 70
}

vol() {
  single_run vol alsamixer
}

# }}}
# {{{ Music
motiv() {
  vol=$(volume_for $0 -5)
  play_dir motiv "$vol" /home/Music --af=haas "--audio-channels=fl-fr" --af=surround --af=scaletempo=scale=1.0:speed=tempo:1.2:stride=70:search=40
}

best() {
  vol=$(volume_for $0 -5)
  play_dir motiv "$vol" /home/Music/best
}

restore_single_run() {
  name=$1
  /usr/bin/tmux has-session -t "$name" || return 1
  /usr/bin/tmux new -AD -s "$name"
  return 0
}
# {{{
safe_extract() {
  [ -n "$1" ] || { echo "No file given!"; return 1; }
  file="$1"

  echo "File mime: $(file -i "$1")"
  echo "File type: $(file "$1")"

  FILETYPE=`file ${file}`
  if echo $FILETYPE | /bin/grep bzip2; then
    "$file" tar jxv
    return $?
  fi

  if echo $FILETYPE | /bin/grep gzip; then
    < "$file" tar zxv
    return $?
  fi

  if echo $FILETYPE | /bin/grep RAR; then
    unrar x "$file"
    return $?
  fi

  if echo $FILETYPE | /bin/grep -w Zip; then
    unzip "$file"
    return $?
  fi

  if echo $FILETYPE | /bin/grep -v "bzip2\|gzip"; then
    return $?
  fi

  echo "Unrecognized file: $FILETYPE, so just copying.."
  mv $file `basename $file .gpg`
  return 1
}

decrypt() {
  ERR_RETURN=1
  # decrypts and unpacks encrypted file
  [ -z "$1" ] || { echo "No file given!"; return 1; }
  [ -z "$TMP" ] || { echo "No TMP env var!"; return 1; }

  TMPFILE="$(mktemp -d "$BIGTMP/decrypt.XXXXXX")" || { echo "failed to create temp file"; return 1; }
  chmod 0700 $TMPFILE
  gpg --quiet --no-verbose --no-greeting --no-secmem-warning --no-mdc-warning -d $1 >> $TMPFILE
  if [ "$?" != "0" ]; then
    rm "$TMPFILE"
    return 1
  fi

  safe_extract "$TMPFILE"
  if [ "$?" != "0" ]; then
    echo "Failed to extract $TMPFILE"
    /bin/rm -f "$1"
    return 1
  fi
  /bin/rm -f "$1"
}

# }}}
#  Compare and remove last if duplicate
cmprm() {
  [ -e "$1" ] || { echo "no such file: $1"; return 1; }
  [ -e "$2" ] || { echo "no such file: $2"; return 1; }
  echo "Comparing:"
  echo "------------"
  echo "$1"
  echo "$2"
  echo "------------"
  ! [ "$1" -ef "$2" ] || { echo "same file!"; return 1; }
  /usr/bin/cmp "$1" "$2" && { echo "IDENTICAL! Removing: $2";  /bin/rm -f "$2"; }
}
# {{{ Admin
upgrade() {
  sudo sh -c 'apt-get update && apt-get -y upgrade --install-suggests --auto-remove'
}
debdu() {
  # show sizes of installed packages
  dpkg-query -W --showformat='${Installed-Size;10}\t${Package}\n' | sort -k1,1n
}
#Shows how much a directory takes up (1 level tree, sorted by size)
dum() {
  du "$@" -x -m --max-depth=1 | sort -n
}

du1 () { du -h --max-depth=1 "$@" | sort -k 1,1hr -k 2,2f; }

# }}}
## {{{ grep - for compatibility with non-Linux system
export GREP='/bin/grep'
[ -x "$GREP" ] || export GREP='/usr/bin/grep'
## }}}
# {{{ single run helper
single_run () {
  name="$1"
  shift
  params="$@"
  if [ -n "$TMUX" ]; then
    if ! /usr/bin/tmux has-session -t "$name"; then
      TMUX= /usr/bin/tmux new -d -s "$name" "$params"
      /usr/bin/tmux switch-client -t "$name"
    else
      echo "NOT IMPLEMENTED WHEN SESSION EXISTS!"
    fi
  else
    /usr/bin/tmux new -AD -s "$name" "$params"
  fi
}
# }}}
# {{{ cp, cd, mkdir, mv, ls etc. very fast
cdls ()
{
  # go to directory and say "ls"
  if test "$#" != "1"; then
    echo "$SHELL : cdls : Wrong number of arguments"
    return 1
  fi
  cd "$1" && ls $LS_OPTIONS
}

cdl ()
{
  # go to directory and say "ls"
  if test "$#" != "1"; then
    echo "$SHELL : cdl : Wrong number of arguments"
    return 1
  fi
  cd "$1" && ls $LS_OPTIONS -lA
}

cpcd ()
{
  # copy files to directory and go there
  # Corresponding trick under tcsh:
  # alias cpcd 'cp \!*;cd \!$'
  cp "$@" && eval LAST_DIR="\${$#}" && cd $LAST_DIR
  unset LAST_DIR
}

mvcd ()
{
  # move files to directory and go there
  mv "$@" && eval LAST_DIR="\${$#}" && cd $LAST_DIR
  unset LAST_DIR
}

mdcd ()
{
  # make directory and go there
  #mkdir "$@" && { shift $(( $#-1 )) ; cd "$1"; }
  mkdir "$@" && eval LAST_DIR="\${$#}" && cd $LAST_DIR
  unset LAST_DIR
}

mdmv ()
{
  # make directory and move stuff there
  # usage: mdmv (newdir) (stuff)
  #mkdir -p "$1" && mv $2 $1
  mkdir "$1" && DIR="$1" && shift && mv "$@" "$DIR"
  unset DIR
}

mdmvcd ()
{
  # make directory, move stuff there, and go there
  # usage: mdmvcd (newdir) (stuff)
  mkdir "$1" && DIR="$1" && shift && mv "$@" "$DIR" && cd $DIR
  unset DIR
}

mdcp ()
{
  # make directory and copy stuff there
  # usage: mdxp (newdir) (stuff)
  mkdir "$1" && DIR="$1" && shift && cp "$@" $DIR
  unset DIR
}

mdcpcd ()
{
  # make directory, copy stuff there, and go there
  # usage: mdcpcd (newdir) (stuff)
  mkdir "$1" && DIR="$1" && shift && cp "$@" $DIR && cd $DIR
  unset DIR
}

# }}}

# {{{ Mailz
MAILZ_CONFIG_DIR="${HOME}/.mutt/mailz"
if [ -d "${MAILZ_CONFIG_DIR}" ]; then

  #mailboxes
  for k in ${MAILZ_CONFIG_DIR}/*; do
    name="$(basename "$k")"
    $name()
    {
      mboxfile="${MAILZ_CONFIG_DIR}/$0"
      sname="${0}_session"
      cmd="TERM=xterm-256color mutt -F ${mboxfile}"
      single_run "$sname" "$cmd"
    }
  done

  mailz()
  {
    for k in ${MAILZ_CONFIG_DIR}/*; do
      mboxfile="$k"
      name="$(basename "$k")"

      sname="${name}_session"
      cmd="TERM=xterm-256color mutt -F ${mboxfile}"
      urxvt -title "$sname" -e tmux new -AD -s $sname "$cmd" &!
    done
  }
fi
#}}}
# {{{ caps
caps() {
  case $1 in
    off)
      xmodmap -e "clear lock"
      xmodmap -e "keycode 0x42 = Escape"
      ;;
    on)
      xmodmap -e "keycode 0x42 = Caps_Lock"
      xmodmap -e "add lock = Caps_Lock"
      ;;
    *)
      echo "Usage: caps {on|off}, turn the caps lock key on or"
      echo "off.  When it is off, the key acts as an Escape key."
      ;;
  esac
}
# }}}

# {{{ useful
readme() {
  local files
  files=(./*([Rr][Ee][Aa][Dd]*[Mm][Ee]|[Ll][Uu][Ee]*[Mm]([Ii][Nn]|)[Uu][Tt])*(ND))
  if (($#files)); then
    $PAGER $files
  else
    print No README files.
  fi
}

cdrun()
{
  dir="$1"
  shift

  pushd "${dir}" > /dev/null
  "$@"
  res=$?
  popd > /dev/null
  return $res
}

archive_git() {
  filename="$(basename $PWD).gitbundle"
  git fsck --strict && git gc --aggressive && git repack -a -d -f -F --window=30 --depth=100 && git bundle create "$filename" --all
}

fetch() {
  each_git_repo fetch "$@"
}

pull() {
  each_git_repo pull "$@"
}

each_git_repo() {
  #default="${HOME}"
  op="$1"
  shift
  #base=${2-$default}
  base="$@"
  [ -z "$base" ] && { echo "Error: you must provide a path"; return 1; }

  for k in $base/**/.git/; do
    dirname=$k..
    echo "GIT: $(realpath "$k")"
    dir="${PWD}"
    cd "$dirname"
    if [ "$op" = "fetch" ]; then
      git fetch
      git log -n 10 --oneline "HEAD..origin/$(git branch --no-color | /bin/grep '*' | awk '{print $2}')"
      echo
    elif [ "$op" = "pull" ]; then
      git pull
    else
      echo "unknown op: $op"
      return 1
    fi
    cd "$dir"
  done
}

set_first_valid_locale()
{
  unset LC_ALL 2> /dev/null
  for loc in $(echo $*); do
    if locale -a | egrep "^${loc}$" > /dev/null 2>&1; then
      for item in LANG LC_ADDRESS LC_COLLATE \
        LC_CTYPE LC_IDENTIFICATION LC_MEASUREMENT \
        LC_MESSAGES LC_MONETARY LC_NAME LC_NUMERIC \
        LC_PAPER LC_TELEPHONE LC_TIME; do
      export $item="${loc}"
    done
    break
  fi
done
}

## {{{ Root
if [ `id -u` = 0 ]; then
  unset LC_ALL
  unset LANG
  set_first_valid_locale "en_US"

  #  unset MAIL MAILCHECK MAILPATH 2> /dev/null
  #  unset IRCNAME IRCSERVER IRCNICK 2> /dev/null
  #  unset NNTPSERVER 2> /dev/null
  unset SSH_AGENT_PID SSH_AUTH_SOCK 2> /dev/null
  #  unset hostnames mailpath 2> /dev/null
  #  unset DICTIONARY
fi
set_first_valid_locale ${MY_LOCALE_ORDER:-en_US.utf8}
# }}}

# g() {
#  if [[ $# == '0' ]]; then
#    git status
#  else
#    case $1 in
#      fuss)
#        shift
#        git rebase -i HEAD~"$1";;
#      *)
  #        git "$@";;
  #    esac
  #  fi
  #}

  #eval `resize < /dev/null`
  ##local default_theme=alanpeabody
  ##local default_theme=afowler

  #export TREE_CHARSET="UTF-8"
  #alias tree='tree -F --dirsfirst -C'

  #export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

  #usernames=${LIMIT_USERNAMES:-$(whoami)}
  #hostnames=${LIMIT_HOSTS:-$(hostname)}
  #compctl -k hostnames telnet rlogin rsh ssh ping ftp ncftp lftp
  #compctl -k hostnames -S ':~/' + -f + -x 'N[-1,@]' -k hostnames -S ':' - 's[]' -u -S '@' -- rcp scp scp1 scp2

  ## automatically remove duplicates from these arrays
  #typeset -U path cdpath fpath manpath
  ## ~/.zshenv
  #emulate zsh
  #umask 022
  ## ulimit -c unlimited

  #WORKSPACE="${HOME}/workspace"
  #CDDB_DIR="${WORKSPACE}/cddb"
  #TMPPREFIX="/tmp/zsh/${USER}"
  ## Set default locale
  #export GTK_IM_MODULE=ibus
  #export XMODIFIERS=@im=ibus
  #export QT_IM_MODULE=ibus
  #export XCURSOR_SIZE=48
  #if [ -f "${HOME}/.bcrc" ]; then
  #  export BC_ENV_ARGS="${HOME}/.bcrc"
  #fi
  # vi: ft=zsh:

ytm() {
  youtube-dl -c -w -x --no-playlist "$@"
}
