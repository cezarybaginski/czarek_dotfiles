# {{{ less
export LESSCHARSET="utf-8"
if type lessfile > /dev/null 2>&1; then
  eval `lessfile`
elif type lesspipe > /dev/null 2>&1; then
  eval `lesspipe`
elif type lesspipe.sh > /dev/null 2>&1; then
  LESSOPEN="|lesspipe.sh %s"
  export LESSOPEN
fi

if [ $EUID = 0 ]; then
  export LESSSECURE="1"
else
  unset LESSSECURE 2> /dev/null
fi

#COLUMNS=80
LESS="-s -n -e -g -i -M -R -W -x8 --shift=8 -F -j.2"
if [ "$COLUMNS" -gt 85 ]; then
  LESS+=" -J -N"
fi
export LESS
export LESSHISTSIZE="1000"
# }}}
