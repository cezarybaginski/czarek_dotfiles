#!/usr/bin/env ruby
# frozen_string_literal: true

require 'pathname'
load Pathname.new(__FILE__).expand_path.dirname + '../check' if $PROGRAM_NAME == __FILE__

require 'fileutils'

require_relative '../lib/hacks/safe_linking'

module MyWorkstation
  class ProperHome
    include Hacks::SafeLinking

    def ok?
      @exception = nil
      data = {
        file: {
          '.local/.keep' => :create,
          '.tmp/.keep' => :create,
          '.fast/.keep' => :create,
          '.cache/.keep' => :create,

          '.cache' => {
            # optional, since not needed until install is finish and vim is started
            { '.config/google-chrome/cache.db' => :auto } => :optional,
            { '.config/google-chrome/Default/Cookies' => :auto } => :optional,
            { '.config/google-chrome/Default/Extension Cookies' => :auto } => :optional,
            # { '.config/google-chrome/Default/Last Session' => :auto } => :optional,
            # { '.config/google-chrome/Default/Last Tabs' => :auto } => :optional,
            #{ '.config/google-chrome/Default/Current Session' => :auto } => :optional,
            #{ '.config/google-chrome/Default/Current Tabs' => :auto } => :optional,
            { '.config/google-chrome/Default/QuotaManager' => :auto } => :optional,
            # { '.config/google-chrome/Default/TransportSecurity' => :auto} => :optional,
            { '.config/google-chrome/Default/Favicons' => :auto } => :optional,
            { '.config/google-chrome/Default/Shortcuts' => :auto } => :optional,
            { '.config/google-chrome/Default/Network Action Predictor' => :auto } => :optional,
            # { '.config/google-chrome/Default/Network Persistent State' => :auto} => :optional,
            # { '.config/VirtualBox/VBoxSVC.log.1' => :auto} => :optional,
            # { '.config/VirtualBox/VBoxSVC.log' => :auto} => :optional,
            # { '.config/VirtualBox/VirtualBox.xml-prev' => :auto} => :optional,
            # { '.lesshst' => :auto} => :optional,
            { '.vim/autoload/pathogen.vim' => '__vim_bundle/vim-pathogen/autoload/pathogen.vim' } => :optional,
            # { '.viminfo' => :auto} => :optional,
            { '.xsession-errors' => :auto } => :optional,
            { '.xsession-errors.old' => :auto } => :optional
          }
        },

        dir: {
          '.private' => {
            { '.aws' => '.aws' } => :optional
          },
          '.fast' => {
            { '.freeciv' => :auto } => :optional,
            { '.var' => :auto } => :optional,
            { 'Dropbox' => :auto } => :optional,
            { '.dropbox-dist' => :auto } => :optional,
            { '.local/share/fonts' => :auto } => :optional,
            { '.local/share/gnome-video-arcade' => :auto } => :optional,
            { '.local/share/icons' => :auto } => :optional,
            { 'Phone' => :auto } => :optional,
            { '.vagrant.d' => :auto } => :optional,
            { 'VirtualBox VMs' => :auto } => :optional
          },

          '.tmp' => {
            { 'workspace/prio_api/tmp' => :auto } => :optional,
            { 'workspace/prio/tmp' => :auto } => :optional,
            { 'workspace/ballad/tmp' => :auto } => :optional,
            { 'workspace/czarek_dotfiles/route_.bin/lib/hacks/coverage' => :auto } => :optional
          },

          '.cache' => {
            { '.vagrant.d/boxes' => :auto } => :optional,
            { '.bundle/cache' => :auto } => :optional,
            { '.npm' => :auto } => :optional,
            { '.config/google-chrome/Default/Extension Rules' => :auto } => :optional,
            { '.config/google-chrome/Default/Extensions' => :auto } => :optional,
            { '.config/google-chrome/Default/Extension State' => :auto } => :optional,
            { '.config/google-chrome/Default/GCM Store' => :auto } => :optional,
            { '.config/google-chrome/Default/GPUCache' => :auto } => :optional,
            { '.config/google-chrome/Default/IndexedDB' => :auto } => :optional,
            { '.config/google-chrome/Default/Local Extension Settings' => :auto } => :optional,
            { '.config/google-chrome/Default/Local Storage' => :auto } => :optional,
            { '.config/google-chrome/Default/Service Worker' => :auto } => :optional,
            { '.config/google-chrome/Default/Session Storage' => :auto } => :optional,
            { '.config/google-chrome/Default/Site Characteristics Database' => :auto } => :optional,
            { '.config/google-chrome/Default/Storage' => :auto } => :optional,
            { '.config/google-chrome/Default/Sync Data' => :auto } => :optional,
            { '.config/google-chrome/Default/Sync Extension Settings' => :auto } => :optional,
            { '.config/google-chrome/Default/VideoDecodeStats' => :auto } => :optional,
            { '.config/google-chrome/ShaderCache' => :auto } => :optional,
            { '.dbus/session-bus' => :auto } => :optional,
            { 'Downloads' => :auto } => :create,
            { '.gem' => :auto } => :optional,
            { 'junk' => :auto } => :create,
            { '.puppetlabs/opt/puppet/cache' => :auto } => :optional,
            { '.rvm' => :auto } => :optional,
            { '.vim/bundle' => :auto } => :optional, # if not set up yet
            { '.zprezto' => :auto } => :optional, # if not set up yet
            { 'node_modules' => :auto } => :optional
          }
        }
      }

      logger =
        Logger.new(STDERR) do |log|
          log.level = Logger::DEBUG
        end

      data.each do |mode, type|
        type.each do |base, options|
          case options
          when :error, :optional, :create
            path = HomePath.new(mode, base, logger)
            logger.info "Checking: #{path.relative_path}"
            NonLink.new(path: path, when_missing: options).check!
          when Hash
            options.each_pair do |args, param|
              from, to = extract_entry(args)
              path = HomePath.new(mode, from, logger)
              logger.info "Checking: #{path.relative_path}"
              target = HomePath.new(mode, File.join(base, to), logger)
              Link.new(path: path, target: target, when_missing: param).check!
            end
          else
            raise NotImplementedError, base.inspect
          end
        end
      end

      true
    rescue RuntimeError => e
      @exception = e
      false
    end

    def extract_entry(link)
      raise ArgumentError, "bad hash: #{link.inspect}" unless link.size == 1

      entry = link.to_a.first
      raise ArgumentError, "bad hash: #{entry.inspect}" unless entry.size == 2

      from, to = entry
      to = "__#{from.downcase.tr('/', '_').sub(/^\./, '')}" if to == :auto

      [from, to]
    end

    def fix
      @exception ? @exception.message : 'unknown error'
    end
  end
end

Workstation.check_with_last_defined if $PROGRAM_NAME == __FILE__
