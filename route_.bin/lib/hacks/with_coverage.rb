gem 'simplecov'
require 'simplecov'

def run(filename)
  SimpleCov.command_name("Within #{filename}")
  SimpleCov.start
  full_path = File.expand_path(filename)
  $0 = full_path
  require full_path
end

run(*ARGV)
