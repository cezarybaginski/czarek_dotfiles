module Hacks
  class HashDB
    class << self
      def verify_or_test(filename)
        if $PROGRAM_NAME == filename
          yield && sign(filename)
        else
          verify(filename)
        end
      end

      def sign(filename)
        #TODO: file locking
        require 'pathname'
        hash_db = db_path
        hash_db.dirname.mkpath

        key = signing_key

        raise "No sigining key!" unless String(key) =~ /^fe\S{10,}$/

        digest = hexdigest(filename)

        data =
          begin
            require 'yaml'
            YAML.load(hash_db.binread)
          rescue Errno::ENOENT
            {}
          end

        with_lock(hash_db) do
          data[RUBY_VERSION] ||= {}
          data[RUBY_VERSION][digest] = true
          hash_db.binwrite(YAML.dump(data))
        end
      end

      def signing_key
        ENV.fetch('MY_UNIT_TEST_SIGNING_KEY')
      end

      SLEEP_IF_LOCKED = 0.3

      def with_lock(path)
        lock_path = path.sub_ext('.yml.lock')
        sleep SLEEP_IF_LOCKED if lock_path.exist?
        raise "File already locked: #{path}" if lock_path.exist?

        lock_path.binwrite(Process.pid)
        yield
        # TODO: potential race condition?
        lock_path.unlink
      end

      def db_path
        require 'pathname'
        Pathname("~/.minitest/signed_as_tested.yml").expand_path
      end

      def verify(filename)
        digest = hexdigest(filename)
        hash_db = db_path
        require 'yaml'
        YAML.load(hash_db.binread).fetch(RUBY_VERSION) do
          raise "#{filename} has not been unit tested (new Ruby version used?)"
        end.fetch(digest) do
          raise "#{filename} has not been unit tested on this machine!"
        end
      end

      def hexdigest(filename)
        require 'openssl'
        OpenSSL::HMAC.hexdigest('sha256', signing_key, IO.binread(filename))
      end
    end
  end
end
