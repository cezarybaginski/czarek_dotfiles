require_relative 'git/capture'
require_relative 'git/commit'

module Hacks
  # Helper for calling git actions
  class Git
    class Error < RuntimeError
    end

    class << self
      def commit(message)
        call('commit', '-m', message)
      end

      def log(*args)
        raise ArgumentError, "bad args: #{args.inspect}" if args.any?(&:nil?)

        call('log', *args)
      end

      def staged
        Capture.staged
      end

      private

      def call(*args)
        raise ArgumentError, "bad args: #{args.inspect}" if args.any?(&:nil?)
        raise ArgumentError, 'no arguments!' if args.empty?
        return if system('git', *args)

        raise Error, "git #{args.inspect} failed"
      end
    end
  end
end
