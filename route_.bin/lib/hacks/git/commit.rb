require 'contracts'

module Hacks
  class Git
    # Represents a valid commit hash
    class Commit
      include Contracts::Core

      class << self
        include Contracts::Core

        def from_last(filename)
          new(last_revlist_for(filename))
        end

        private

        def last_revlist_for(filename)
          only_one(Capture.head_rev_list_for_file(filename.to_s))
        end

        Contract [String] => String
        def only_one(entries)
          entries.first
        end
      end

      REGEXP = /^\h{4,}$/

      Contract REGEXP => Contracts::Any
      def initialize(commit)
        @commit = commit
      end

      def to_s
        @commit
      end
    end
  end
end

module Hacks
  require_relative '../hash_db'
  HashDB.verify_or_test(__FILE__) do
    gem 'minitest', '5.14.4'
    require 'minitest'
    require 'minitest/mock'

    gem 'minitest-stub-const', '0.6'
    require 'minitest/stub_const'

    # Test the above class
    class TestCommit < Minitest::Test
      def test_fetches_last_commit_for_file
        mock = Minitest::Mock.new
        mock.expect(:head_rev_list_for_file, ['0deadbeef'], ['foobar.rb'])

        Hacks::Git.stub_const(:Capture, mock) do
          assert_equal '0deadbeef', Hacks::Git::Commit.from_last('foobar.rb').to_s
        end

        assert_mock mock
      end
    end
    Minitest.run
  end
end
