require 'contracts'

module Hacks
  class Git
    # Captures output of git commands
    class Capture
      include Contracts::Core
      include Contracts::Builtin

      # Generic capture error
      class FailedCommand < RuntimeError
        def initialize(cmd)
          super("command failed: #{cmd.inspect}")
        end
      end

      class << self
        def rev_list(*args)
          entries('rev-list', *args)
        end

        def head_rev_list_for_file(filename)
          rev_list('-n', '1', 'HEAD', '--', filename)
        end

        def staged
          diff('--staged', '--name-only')
        end

        private

        def diff(*args)
          entries('diff', *args)
        end

        def entries(*args)
          capture!('git', *args).lines.map(&:chomp)
        end

        # just to help with contracts
        def capture!(*args)
          do_capture(args)
        end

        require 'shellwords'
        Contract ArrayOf[String] => String
        def do_capture(args)
          cmd = args.shelljoin
          `#{cmd}`.tap do
            require 'English'
            raise FailedCommand, cmd unless $CHILD_STATUS.exitstatus.zero?
          end
        end
      end
    end
  end
end
