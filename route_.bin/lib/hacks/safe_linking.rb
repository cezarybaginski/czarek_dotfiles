# frozen_string_literal: true

require 'logger'

module Hacks
  module SafeLinking
    class HomePath
      def self.home
        @home ||= Pathname.new('~').expand_path
      end

      attr_reader :typename

      def initialize(type, item, logger)
        raise ArgumentError, "Bad type: #{type.inspect}" unless %i(file dir).include?(type)
        raise ArgumentError, "bad item: #{item.inspect}" unless item.is_a?(String)
        raise ArgumentError, "no logger" if logger.nil?

        @logger = logger
        raise ArgumentError, "can't be absolute: #{item}!" if Pathname.new(item).absolute?

        path = self.class.home + item

        @path = path

        @type = type
        @typename = { dir: 'directory', file: 'file' }.fetch(type)
        @dir = (type == :dir)

        raise "Not a #{typename}: #{path}" if !symlink? && path.exist? && !same_type?(path)
      end

      def ==(other)
        to_path == other.to_path
      end

      def relative_path
        @relative_path ||= create_same(path)
      end

      def readlink
        @readlink ||= create_same(path.readlink)
      end

      # used during cleanup
      def realpath
        @realpath ||= create_same(path.realpath)
      end

      def symlink?
        @symlink ||= path.symlink?
      end

      def symlink_now?
        path.symlink?
      end

      # broken links return false on exist?, so we need a new method instead
      def missing?
        @missing ||= !symlink? && !path.exist?
      end

      def unlink_broken_symlink
        raise ArgumentError, 'not a symlink!' unless path.symlink?

        logger.warn "Fixing broken link: #{path} (pointing to #{path.readlink})"
        path.unlink
      end

      # called from 2 places (both good outcomes)
      def create_symlink_to(target)
        logger.info "Linking #{path} => #{target}"
        FileUtils.ln_s target.to_path, path
      end

      # TODO: link files if identical

      def ensure_exists
        if dir?
          return if path.directory?
          raise "Not a dir: #{path}" if path.exist? || path.symlink?

          logger.info "Creating dir: #{path}"
          path.mkpath
        else
          return if path.file?
          raise "Not a file: #{path}" if path.exist? || path.symlink?

          logger.info "Creating empty file: #{path}"

          # TODO: NOT TESTED!
          dir = path.dirname
          begin
            dir.realpath
          rescue Errno::ENOENT
            dir.unlink if dir.symlink?
            dir.mkpath
          end

          FileUtils.touch(path)
        end
      end

      def still_exists_after_cleanup?
        begin
          real_path = path.realpath

          return true unless real_path.empty?

          dir? ? rmpath(path) : path.unlink
        rescue Errno::ENOENT, Errno::EBUSY, Errno::ENOTEMPTY
          unlink_broken_symlink if path.symlink?
        end

        false
      end

      def rmpath(path)
        loop do
          return unless path.rmdir

          path = path.dirname
        end
      rescue Errno::EACCES, Errno::ENOTEMPTY
      end

      def move_to_become(target)
        logger.info "Moving/migrating: #{path} to #{target}"
        dst_dir = target.to_path.dirname
        dst_dir.mkpath
        FileUtils.mv(path.realpath.to_s, target.to_path, verbose: true)
      end

      def to_path
        path
      end

      def to_s
        path.to_s
      end

      private

      attr_reader :home
      attr_reader :type
      attr_reader :path
      attr_reader :logger

      def create_same(path)
        # NOTE: readlink can return relative path
        relative = path.absolute? ? path.relative_path_from(self.class.home) : path
        HomePath.new(type, relative.to_s, logger)
      end

      def dir?
        @dir
      end

      def same_type?(path)
        dir? ? path.directory? : path.file?
      end
    end

    class Base
      def initialize(path:, when_missing:)
        raise ArgumentError, "bad path type: #{path.inspect}" unless path.is_a?(HomePath)
        raise ArgumentError, "bad when_missing value: #{when_missing.inspect}"  unless %i(create optional error).include?(when_missing)
        @path = path
        @when_missing = when_missing
      end

      attr_reader :path
      attr_reader :when_missing
    end

    class Link < Base
      def initialize(path:, target:, when_missing:)
        #raise ArgumentError, "bad path type: #{path.inspect}" unless path.is_a?(HomePath)
        raise ArgumentError, "bad target type: #{path.inspect}" unless target.is_a?(HomePath)
        raise ArgumentError, "bad when_missing value: #{when_missing.inspect}"  unless %i(create optional error).include?(when_missing)

        target_type = target.typename
        path_type = path.typename
        raise "#{path} is a #{path_type} while #{target} is a #{target_type}" unless path_type == target_type

        super(path: path, when_missing: when_missing)

        @target = target
      end

      def check!
        target_exists = target.still_exists_after_cleanup?
        path_exists = path.still_exists_after_cleanup?

        both = path_exists && target_exists
        neither = !path_exists && !target_exists

        return if path_exists && path.symlink_now? && fail_unless_proper_symlink && target_exists

        raise error_both_nonempty if both

        return if neither && !create_if_allowed

        path.move_to_become(target) unless target_exists
        path.create_symlink_to(target)
      end

      private

      def create_if_allowed
        case when_missing
        when :error then raise "Must exist as nonempty: #{path}"
        when :optional then false
        when :create
          path.ensure_exists
          true
        end
      end

      def fail_unless_proper_symlink
        (path_readlink = path.readlink) == target || fail_since_linked_elsewhere(path_readlink)
      end

      def error_both_nonempty
        target_realpath = target.realpath
        # TODO: check hardlinks?
        raise "Both source & target are #{path}" if path == target_realpath

        raise "Both #{path} and #{target} are nonempty and exist independently"
      end

      def fail_since_linked_elsewhere(path_readlink)
        raise error_linked_elsewhere_message(path_readlink)
      end

      def error_linked_elsewhere_message(path_readlink)
        # We could migrate, but it may not be what the user wants)
        rel_path = path.relative_path.to_s
        rel_badlink = path_readlink.relative_path.to_s
        rel_target = target.relative_path.to_s

        msg = +"Existing link:  #{rel_path} => #{rel_badlink}\n"
        msg << " instead of:    #{rel_path} => #{rel_target}\n"
        msg << "Suggestion: mv #{rel_badlink} #{rel_target}"
        raise msg
      end

      attr_reader :target
    end

    class NonLink < Base
      def initialize(path:, when_missing:)
        #raise ArgumentError, "bad path type: #{path.inspect}" unless path.is_a?(HomePath)
        raise ArgumentError, "bad when_missing value: #{when_missing.inspect}"  unless %i(create optional error).include?(when_missing)
        super(path: path, when_missing: when_missing)
      end

      def check!
        raise "Needs to be regular #{typename}, not symlink: #{path}" if path.symlink?

        return unless path.missing?

        case when_missing
        when :create
          path.ensure_exists
        when :optional
        when :error
          raise "Must exist: #{path}"
        end
      end
    end
  end
end

if $PROGRAM_NAME == __FILE__

  gem 'minitest', '~> 5.11'
  require 'minitest'
  require 'minitest/mock'

  gem 'minitest-stub-const', '~> 0.6'
  require 'minitest/stub_const'

  require 'color_pound_spec_reporter'
  Minitest::Reporters.use! [ColorPoundSpecReporter.new]

  module MyHacks
    class SaneBacktraceFilter < Minitest::ExtensibleBacktraceFilter
      # :nocov:
      def filter(backtrace)
        pwd = Dir.pwd
        bt = without_minitest_plugins { super }
        bt = bt.dup if bt.frozen?
        bt.reject! { |line| filters?(line) }
        bt.map! { |line| relative_path(pwd, line) }
      end

      private

      def relative_path(dir, line)
        line.sub(%r{^#{dir}\/}, '')
      end

      def without_minitest_plugins
        old_filters = @filters
        @filters = [@filters.first]
        yield.tap do
          @filters = old_filters
        end
      end
      # :nocov:
    end
  end

  Minitest.backtrace_filter = MyHacks::SaneBacktraceFilter.default_filter

  Minitest.backtrace_filter.add_filter(%r{^/gems/contracts-})
  Minitest.backtrace_filter.add_filter(%r{^/usr/local/rvm/})
  Minitest.backtrace_filter.add_filter(/with_coverage\.rb/)

  require 'pathname'

  module CommonTestStuff
    def without_filesystem
      do_not_call = Minitest::Mock.new
      Object.stub_const(:File, do_not_call) do
        Object.stub_const(:FileUtils, do_not_call) do
          Object.stub_const(:Pathname, do_not_call) do
            yield
          end
        end
      end
      assert_mock do_not_call
    end

    def scenario_when(proper: true)
      mock = Minitest::Mock.new
      mock.expect(:is_a?, true, [Hacks::SafeLinking::HomePath])

      # construction
      mock.expect(:typename, 'directory')

      mock.expect(:still_exists_after_cleanup?, proper)
      mock
    end
  end

  class TestNonLinks < Minitest::Test
    include CommonTestStuff

    def setup
      @logger = Minitest::Mock.new
      @logger.expect(:debug, true) { true }
      @logger.expect(:info, true) { true }
      @logger.expect(:warn, true) { true }
    end

    def test_create_non_existing_file_with_path
      without_filesystem do
        when_missing = :create

        path = Minitest::Mock.new
        path.expect(:is_a?, true, [Hacks::SafeLinking::HomePath])
        path.expect(:missing?, true)
        path.expect(:symlink?, false)
        path.expect(:ensure_exists, true)

        Hacks::SafeLinking::NonLink.new(path: path, when_missing: when_missing).check!
        assert_mock path
      end
    end

    def test_optional_non_existing_file_with_path
      without_filesystem do
        when_missing = :optional

        path = Minitest::Mock.new
        path.expect(:is_a?, true, [Hacks::SafeLinking::HomePath])
        path.expect(:missing?, true)
        path.expect(:symlink?, false)

        Hacks::SafeLinking::NonLink.new(path: path, when_missing: when_missing).check!
        assert_mock path
      end
    end

    def test_error_non_existing_file_with_path
      without_filesystem do
        when_missing = :error

        path = Minitest::Mock.new
        path.expect(:is_a?, true, [Hacks::SafeLinking::HomePath])
        path.expect(:missing?, true)
        path.expect(:symlink?, false)

        err = assert_raises RuntimeError do
          Hacks::SafeLinking::NonLink.new(path: path, when_missing: when_missing).check!
        end

        assert_match(/Must exist/, err.message)
        assert_mock path
      end
    end
  end

  class TestSafeLinking < Minitest::Test
    include CommonTestStuff

    # TF
    def test_links_when_strict_and_symlink_missing
      without_filesystem do
        when_missing = :error

        target = scenario_when(proper: true)
        path = scenario_when(proper: false)

        path.expect(:create_symlink_to, nil, [target])

        Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!

        assert_mock target
        assert_mock path
      end
    end

    # TF
    def test_migrates_when_strict_and_improper_path_and_proper_target
      without_filesystem do
        when_missing = :error

        target = scenario_when(proper: true)
        path = scenario_when(proper: false)

        path.expect(:create_symlink_to, nil, [target])

        Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!

        assert_mock target
        assert_mock path
      end
    end

    # FF
    def test_fails_if_strict_and_both_missing
      without_filesystem do
        when_missing = :error

        target = scenario_when(proper: false)
        path = scenario_when(proper: false)

        err = assert_raises RuntimeError do
          Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!
        end

        assert_match(/Must exist/, err.message)
        assert_mock target
        assert_mock path
      end
    end

    # FF
    def test_creates_if_asked_and_both_missing
      without_filesystem do
        when_missing = :create

        target = scenario_when(proper: false)
        path = scenario_when(proper: false)

        # after creating
        path.expect(:ensure_exists, nil)
        path.expect(:move_to_become, nil, [target])
        path.expect(:create_symlink_to, nil, [target])

        Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!

        assert_mock target
        assert_mock path
      end
    end

    # FTn
    def test_when_strict_and_path_and_no_proper_target
      without_filesystem do
        when_missing = :error # anything in this case

        target = scenario_when(proper: false)
        path = scenario_when(proper: true)

        path.expect(:symlink_now?, false)

        path.expect(:move_to_become, nil, [target])
        path.expect(:create_symlink_to, nil, [target])

        Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!

        assert_mock target
        assert_mock path
      end
    end

    # FTs
    def test_fails_when_strict_and_path_links_to_something_else_and_improper_target
      without_filesystem do
        when_missing = :error

        target = scenario_when(proper: false)
        path = scenario_when(proper: true)

        # in error messages
        target_rel_path = Minitest::Mock.new
        target.expect(:relative_path, target_rel_path)

        path.expect(:symlink_now?, true)
        path_rel_path = Minitest::Mock.new
        path.expect(:relative_path, path_rel_path)

        path_linktarget_path_rel_path = Minitest::Mock.new
        path_linktarget_path = Minitest::Mock.new
        path_linktarget_path.expect(:relative_path, path_linktarget_path_rel_path)
        path_linktarget_path.expect(:==, false, [target])
        path.expect(:readlink, path_linktarget_path)

        err = assert_raises RuntimeError do
          Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!
        end
        assert_match(/Suggestion: mv/, err.message)

        assert_mock target
        assert_mock target_rel_path
        assert_mock path
        assert_mock path_rel_path
      end
    end

    # TTs
    def test_does_nothing_if_already_properly_linked
      without_filesystem do
        when_missing = :error

        target = scenario_when(proper: true)
        path = scenario_when(proper: true)

        path.expect(:symlink_now?, true)
        path_linktarget_path = Minitest::Mock.new
        path_linktarget_path.expect(:==, true, [target])
        path.expect(:readlink, path_linktarget_path)

        Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!
        assert_mock target
        assert_mock path
        assert_mock path_linktarget_path
      end
    end

    # TTn
    def test_fails_if_strict_and_both_proper
      without_filesystem do
        when_missing = :error

        target = scenario_when(proper: true)
        path = scenario_when(proper: true)

        path.expect(:symlink_now?, false)

        # for check for same entry
        target_realpath = Minitest::Mock.new
        target.expect(:realpath, target_realpath)
        path.expect(:==, false, [target_realpath])

        # extra
        target.expect(:to_s, 'bar')

        # extra
        path.expect(:to_s, 'foo')

        err = assert_raises RuntimeError do
          Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!
        end
        assert_match(/Both foo and bar are nonempty and exist independently/, err.message)
        assert_mock target
        assert_mock target_realpath
        assert_mock path
      end
    end

    # TTx
    def test_fails_if_strict_and_symlinked_path_to_other_and_proper_target
      without_filesystem do
        when_missing = :error

        target = scenario_when(proper: true)
        path = scenario_when(proper: true)

        path.expect(:symlink_now?, true)

        path_linktarget_path = Minitest::Mock.new
        path_linktarget_path.expect(:==, false, [target])
        path_linktarget_path_rel_path = Minitest::Mock.new
        path_linktarget_path.expect(:relative_path, path_linktarget_path_rel_path)
        path.expect(:readlink, path_linktarget_path)

        target_rel_path = Minitest::Mock.new
        target.expect(:relative_path, target_rel_path)

        path_rel_path = Minitest::Mock.new
        path_rel_path.expect(:to_s, '.foo')
        path.expect(:relative_path, path_rel_path)

        path_linktarget_path_rel_path.expect(:to_s, '.baz')
        target_rel_path.expect(:to_s, '.cache/__bar')

        err = assert_raises RuntimeError do
          Hacks::SafeLinking::Link.new(path: path, target: target, when_missing: when_missing).check!
        end
        assert_match(%r{Existing link: \s* .foo => .baz\n instead of: \s* .foo => .cache/__bar\n}, err.message)
        assert_mock target
        assert_mock path
        assert_mock path_rel_path
      end
    end
  end

  if /docker/.match?(IO.binread('/proc/1/cgroup'))
    class TestWithIntegration < Minitest::Test
      def setup
        @logger = Logger.new(STDOUT).tap { |log| log.level = Logger::ERROR }
      end

      def teardown
        path = Pathname.new("backup.#{Time.now.to_f}")
        path.mkdir
        %w[.cache .foo bar].each do |item|
          FileUtils.mv(item, path) if File.exist?(item) || File.symlink?(item)
        end
      rescue Exception => ex
        # :nocov:
        STDERR.puts "Exception in teardown: #{ex}"
        STDERR.puts ex.backtrace
        # :nocov:
      end

      attr_reader :logger

      def test_links_if_target_exists
        FileUtils.mkdir('.cache')
        File.write('.cache/__foo', 'hello') # anything nonempty

        homepath = Hacks::SafeLinking::HomePath.new(:file, '.foo', logger)
        hometarget = Hacks::SafeLinking::HomePath.new(:file, '.cache/__foo', logger)
        Hacks::SafeLinking::Link.new(path: homepath, target: hometarget, when_missing: :error).check!

        path = Pathname.new('.foo')
        assert path.exist?
        assert path.symlink?
        assert_equal File.expand_path('~/.cache/__foo'), path.readlink.to_s
      end

      def test_creates_dir_if_asked
        homepath = Hacks::SafeLinking::HomePath.new(:dir, '.foo', logger)
        hometarget = Hacks::SafeLinking::HomePath.new(:dir, '.cache/__foo', logger)
        Hacks::SafeLinking::Link.new(path: homepath, target: hometarget, when_missing: :create).check!

        path = Pathname.new('.foo')
        assert path.exist?
        assert path.directory?
        assert path.symlink?
        assert_equal File.expand_path('~/.cache/__foo'), path.readlink.to_s
        assert File.directory?(File.expand_path('~/.cache/__foo'))
      end

      def test_creates_nested_file_if_asked
        homepath = Hacks::SafeLinking::HomePath.new(:file, '.foo/.keep', logger)
        hometarget = Hacks::SafeLinking::HomePath.new(:file, '.cache/__keep', logger)
        Hacks::SafeLinking::Link.new(path: homepath, target: hometarget, when_missing: :create).check!

        path = Pathname.new('.foo/.keep')
        assert path.exist?
        assert path.file?
        assert path.symlink?

        target = File.expand_path('~/.cache/__keep')
        assert_equal target, path.readlink.to_s
        assert File.file?(target)
        assert !File.symlink?(target)
      end

      def test_removes_broken_symlink
        FileUtils.ln_s('bar', '.foo')
        homepath = Hacks::SafeLinking::HomePath.new(:dir, '.foo', logger)
        hometarget = Hacks::SafeLinking::HomePath.new(:dir, '.cache/__foo', logger)
        Hacks::SafeLinking::Link.new(path: homepath, target: hometarget, when_missing: :optional).check!

        path = Pathname.new('.foo')
        assert !path.exist?
        assert !path.symlink?

        target = Pathname.new('.cache/__foo')
        assert !target.exist?
        assert !target.symlink?
      end

      def test_replaces_broken_target_with_existing_nonempty_dir
        # create broken symlink
        FileUtils.mkdir('.cache')
        FileUtils.ln_s('bar', '.cache/__foo')

        # create non-empty dir
        FileUtils.mkdir('.foo')
        File.write('.foo/a.txt', 'hello')

        homepath = Hacks::SafeLinking::HomePath.new(:dir, '.foo', logger)
        hometarget = Hacks::SafeLinking::HomePath.new(:dir, '.cache/__foo', logger)
        Hacks::SafeLinking::Link.new(path: homepath, target: hometarget, when_missing: :optional).check!

        path = Pathname.new('.foo')
        assert path.exist?
        assert path.symlink?
        assert path.readlink == Pathname.new('~/.cache/__foo').expand_path

        assert File.exist?('.cache/__foo/a.txt')

        target = Pathname.new('.cache/__foo')
        assert target.exist?
        assert !target.symlink?
      end

      def test_removes_both_empty_dirs_if_optional
        FileUtils.mkpath('.cache/__foo')
        FileUtils.mkpath('.foo')

        homepath = Hacks::SafeLinking::HomePath.new(:dir, '.foo', logger)
        hometarget = Hacks::SafeLinking::HomePath.new(:dir, '.cache/__foo', logger)
        Hacks::SafeLinking::Link.new(path: homepath, target: hometarget, when_missing: :optional).check!

        assert !File.exist?('cache/__foo')
        assert !File.symlink?('cache/__foo')

        assert !File.exist?('cache')
        assert !File.symlink?('cache')

        assert !File.exist?('.foo')
        assert !File.symlink?('.foo')
      end
    end
  end

  Minitest.run
end
