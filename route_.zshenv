# Ensure that a non-login, non-interactive shell has a defined environment.
if [[ ( "$SHLVL" -eq 1 && ! -o LOGIN ) && -s "${ZDOTDIR:-$HOME}/.zprofile" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprofile"
fi


if grep -q "/dev/zram0 ${HOME}/.tmp ext2" /proc/mounts; then
  RAM_TMP="${HOME}/.tmp"
else
  RAM_TMP="${TMPPREFIX:-/tmp/$USER}.noram"
fi

TMPPREFIX="${TMPPREFIX:-${RAM_TMP}/}_zsh.${USER}"

LIBRARIAN_PUPPET_TMP="${RAM_TMP}/_librarian_puppet"
BOOTSNAP_CACHE_DIR="${RAM_TMP}/_bootsnap_cache"
RAILS_TMP="${RAM_TMP}/_rails"

MANWIDTH=80

#export PATH="/home/cezary-im/.ebcli-virtual-env/executables:$PATH"
